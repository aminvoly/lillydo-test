<?php


namespace App\Listeners;


use App\Events\AddressBookEvent;
use App\Events\AddressBookDeleted;
use App\Service\ImageUploader;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Filesystem\Filesystem;

class AddressBookCreatedListener implements EventSubscriberInterface
{

    /**
     * @var ImageUploader
     */
    private $imageUploader;


    public function __construct(ImageUploader $imageUploader)
    {
        $this->imageUploader = $imageUploader;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            AddressBookEvent::class => ['manageImage'],
            AddressBookDeleted::class => ['deleteImage'],
        ];
    }

    public function manageImage(AddressBookEvent $addressBook)
    {
        $image = $addressBook->getImage();
        if ($addressBook->getImage() && $addressBook->getAddressBook()->getPicture()) {
            $this->imageUploader->removeOldUploadedPicture($addressBook->getAddressBook()->getPicturePath());
        }

        if ($image) {
            $imageFileName = $this->imageUploader->upload($image);
            $addressBook->getAddressBook()->setPicture($imageFileName);
        }
    }

    public function deleteImage(AddressBookDeleted $addressBookDeleted)
    {
        $this->imageUploader->removeOldUploadedPicture($addressBookDeleted->getImage());
    }
}
