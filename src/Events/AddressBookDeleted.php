<?php


namespace App\Events;

use App\Entity\AddressBook;
use Symfony\Component\EventDispatcher\Event;

class AddressBookDeleted extends Event
{
    /**
     * @var AddressBook
     */
    private $image;


    public function __construct(string $image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

}
