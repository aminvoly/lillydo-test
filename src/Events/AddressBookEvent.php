<?php


namespace App\Events;

use App\Entity\AddressBook;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AddressBookEvent extends Event
{
    /**
     * @var AddressBook
     */
    private $addressBook;

    private $image;

    public function __construct(AddressBook $addressBook, $image)
    {
        $this->addressBook = $addressBook;
        $this->image = $image;
    }

    /**
     * @return AddressBook
     */
    public function getAddressBook(): AddressBook
    {
        return $this->addressBook;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }
}
