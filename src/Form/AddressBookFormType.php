<?php

namespace App\Form;

use App\Entity\AddressBook;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Image;

class AddressBookFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', null, ['empty_data' => ''])
            ->add('lastName', null, ['empty_data' => ''])
            ->add('street', null, ['empty_data' => ''])
            ->add('number', null, ['empty_data' => ''])
            ->add('zip', null, ['empty_data' => ''])
            ->add('city', null, ['empty_data' => ''])
            ->add('country', null)
            ->add('phone', null, ['empty_data' => ''])
            ->add(
                'birthday',
                DateType::class,
                array(
                    'widget'     => 'single_text',
                    'format'     => 'yyyy-MM-dd',
                    'empty_data' => '',
                )
            )
            ->add('email', null, ['empty_data' => ''])
            ->add(
                'imageFile',
                FileType::class,
                [
                    'label'       => 'Upload picture',
                    'attr'        => ['class' => 'bg-light p-2'],
                    'mapped'      => false,
                    'required'    => false,
                    'constraints' => new Image(
                        ['maxSize' => '5M']
                    ),
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'csrf_token_id' => 'kachal',
                'data_class'    => AddressBook::class,
            ]
        );
    }
}
