<?php

namespace App\Service;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class ImageUploader
 * @package App\Service
 */
class ImageUploader
{
    /**
     * @var
     */
    private $targetDirectory;
    /**
     *
     */
    const MAIN_DIRECTORY_PATH = 'uploads';
    /**
     *
     */
    const IMAGE_PATH = 'images';

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * ImageUploader constructor.
     * @param $targetDirectory
     * @param Filesystem $filesystem
     */
    public function __construct($targetDirectory, Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
        $this->targetDirectory = $targetDirectory;
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function upload(UploadedFile $file)
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = transliterator_transliterate(
            'Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()',
            $originalFilename
        );
        $fileName = $safeFilename . '-' . uniqid() . '.' . $file->guessExtension();

        try {
            $file->move($this->getTargetDirectory() . DIRECTORY_SEPARATOR . self::IMAGE_PATH, $fileName);
        } catch (FileException $e) {

        }

        return $fileName;
    }

    /**
     * @param string $path
     */
    public function removeOldUploadedPicture(string $path)
    {
        try {
            $this->filesystem->remove($path);
        } catch (\Exception $exception) {

        }
    }

    /**
     * @return string
     */
    public function getTargetDirectory()
    {
        return $this->targetDirectory . DIRECTORY_SEPARATOR . self::MAIN_DIRECTORY_PATH;
    }
}
