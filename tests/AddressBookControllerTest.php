<?php

namespace App\Tests;

use App\Entity\AddressBook;
use Faker\Factory;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;

class AddressBookControllerTest extends WebTestCase
{
    private $faker;
    private $client;

    public function setUp()
    {
        $this->client = static::createClient();
        $this->faker = Factory::create();
        self::bootKernel();
        $this->truncateEntities([AddressBook::class]);
    }

    public function testIndex()
    {
        $this->assertTrue(true);;
        $crawler = $this->client->request('GET', '/');
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $this->assertStringContainsString('Contacts list', $crawler->filter('h3')->text());
    }

    public function testCreate()
    {
        $crawler = $this->client->request('GET', "/address/book/create");
        $form = $crawler->selectButton('Submit')->form();
        $this->populateData($form, $crawler);
        $this->client->submit($form);
        $this->assertSame(302, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->client->getResponse()->isRedirect('/'));
        $crawler = $this->client->followRedirect();
        $this->assertStringContainsString(
            'New record has been added successfully!',
            $crawler->filter('div.alert-success')->text()
        );
    }

    public function testUpdate()
    {
        /** @var AddressBook $addressBook */
        $addressBook = $this->mockAddressBook();
        $crawler = $this->client->request('GET', "/address/book/" . $addressBook->getId() . "/edit");
        $form = $crawler->selectButton('Submit')->form();
        $this->populateData($form, $crawler);
        $this->client->submit($form);
        $this->assertSame(302, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->client->getResponse()->isRedirect('/'));
        $crawler = $this->client->followRedirect();
        $this->assertStringContainsString('Your changes were saved!', $crawler->filter('div.alert-success')->text());
    }

    private function mockAddressBook()
    {
        $addressBook = new AddressBook();
        $addressBook->setFirstName($this->faker->firstName);
        $addressBook->setLastName($this->faker->lastName);
        $addressBook->setStreet($this->faker->streetAddress);
        $addressBook->setNumber($this->faker->randomNumber());
        $addressBook->setZip($this->faker->numberBetween(100, 999));
        $addressBook->setCity($this->faker->city);
        $addressBook->setCountry($this->faker->country);
        $addressBook->setPhone($this->faker->randomNumber());
        $addressBook->setBirthday(new \DateTimeImmutable());
        $addressBook->setEmail($this->faker->email);
        static::$kernel = static::createKernel();
        static::$kernel->boot();

        $this->getEntityManager()->persist($addressBook);
        $this->getEntityManager()->flush();

        return $addressBook;
    }

    private function populateData($form, Crawler $crawler)
    {
        $form["address_book_form[firstName]"] = $this->faker->firstName;
        $form["address_book_form[lastName]"] = $this->faker->lastName;
        $form["address_book_form[street]"] = $this->faker->streetAddress;
        $form["address_book_form[number]"] = $this->faker->randomNumber();
        $form["address_book_form[zip]"] = $this->faker->numberBetween(100, 999);
        $form["address_book_form[city]"] = $this->faker->city;
        $form["address_book_form[country]"] = $this->faker->country;
        $form["address_book_form[phone]"] = $this->faker->randomNumber();
        $form["address_book_form[birthday]"] = $this->faker->date('Y-m-d');
        $form["address_book_form[email]"] = $this->faker->email;
        $form["address_book_form[imageFile]"]->upload(dirname(__FILE__) . '/2.jpg');
        $extract = $crawler->filter('input[name="address_book_form[_token]"]')
            ->extract(array('value'));
        $csrf_token = $extract[0];
        $form["address_book_form[_token]"] = $csrf_token;
    }

    private function truncateEntities(array $entities)
    {
        $connection = $this->getEntityManager()->getConnection();
        $databasePlatform = $connection->getDatabasePlatform();
        if ($databasePlatform->supportsForeignKeyConstraints()) {
            $connection->query('SET FOREIGN_KEY_CHECKS=0');
        }
        foreach ($entities as $entity) {
            $query = $databasePlatform->getTruncateTableSQL(
                $this->getEntityManager()->getClassMetadata($entity)->getTableName()
            );
            $connection->executeUpdate($query);
        }
        if ($databasePlatform->supportsForeignKeyConstraints()) {
            $connection->query('SET FOREIGN_KEY_CHECKS=1');
        }
    }

    private function getEntityManager()
    {
        return self::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }
}
