# Installation
Please Follow the steps to run the project.
```bash
Make a copy of .env.examle and name it .env
composer install
./bin/console make:migration
./bin/console doctrine:migrations:migrate
```
# To run the tests
```bash
./bin/phpunit
``` 
# Code details
```
All the requests for edit and create are handled via symfony form.
I have created two events with one handler(subscriber) for both of them, 
and the subcriber handles both image upload and deleting image when the record is deleted.
```
 
# Packages
```bash
The installed some packages, most of them are for dev environment, the only one
which is not is knp paginator bundle, which I have used for pagination.
