<?php

namespace App\Controller;

use App\Entity\AddressBook;
use App\Events\AddressBookEvent;
use App\Events\AddressBookDeleted;
use App\Form\AddressBookFormType;
use App\Service\ImageUploader;
use Doctrine\DBAL\Exception\DatabaseObjectNotFoundException;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AddressBookController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var ImageUploader
     */
    private $imageUploader;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(
        EntityManagerInterface $entityManager,
        ImageUploader $imageUploader,
        EventDispatcherInterface $dispatcher
    )
    {

        $this->entityManager = $entityManager;
        $this->imageUploader = $imageUploader;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @Route("/", name="address_book")
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function index(PaginatorInterface $paginator, Request $request)
    {
        $results = $this->entityManager->getRepository(AddressBook::class)->findAllAddressBooks();
        $addressBooks = $paginator->paginate($results, $request->query->getInt('page', 1));

        return $this->render(
            'address_book/index.html.twig',
            compact('addressBooks')
        );
    }

    /**
     * @Route("/address/book/create", name="address_book_create")
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $addressBook = new AddressBook();
        $form = $this->createForm(AddressBookFormType::class, $addressBook);
        $result = $this->handleRequest($form, $request, $addressBook);
        if ($result) {
            $this->addFlash(
                'notice',
                'New record has been added successfully!'
            );

            return $this->redirectToRoute('address_book');
        }
        return $this->render(
            'address_book/create.html.twig',
            [
                'addressBookForm' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/address/book/{addressBook}/edit", name="address_book_edit")
     * @param AddressBook $addressBook
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function edit(AddressBook $addressBook, Request $request)
    {
        $form = $this->createForm(
            AddressBookFormType::class,
            $addressBook,
            ['method' => 'PUT']
        );
        $result = $this->handleRequest($form, $request, $addressBook);
        if ($result) {
            $this->addFlash('notice', 'Your changes were saved!');

            return $this->redirectToRoute('address_book');
        }

        return $this->render('address_book/create.html.twig', ['addressBookForm' => $form->createView()]);
    }

    /**
     * @Route("/address/book/{addressBook}", name="address_book_show",methods={"GET"})
     * @param AddressBook $addressBook
     * @return Response
     */
    public function show(AddressBook $addressBook)
    {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('address_book_delete', ['addressBook' => $addressBook->getId()]))
            ->setMethod('DELETE')
            ->getForm()->createView();

        return $this->render('address_book/show.html.twig', compact('addressBook', 'form'));
    }

    /**
     * @Route("/address/book/{addressBook}", name="address_book_delete",methods={"DELETE"})
     * @param AddressBook $addressBook
     * @return RedirectResponse
     */
    public function delete(AddressBook $addressBook)
    {
        try {
            $this->entityManager->remove($addressBook);
            $this->dispatcher->dispatch(
                AddressBookDeleted::class,
                new AddressBookDeleted($addressBook->getPicturePath())
            );
            $this->entityManager->flush();
            $this->addFlash(
                'notice',
                'Record deleted successfully!'
            );

            return $this->redirectToRoute('address_book');
        } catch (DatabaseObjectNotFoundException $exception) {

        }
    }

    private function handleRequest(FormInterface $form, Request $request, AddressBook $addressBook)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $image = $form['imageFile']->getData();
            $this->dispatcher->dispatch(AddressBookEvent::class, new AddressBookEvent($addressBook, $image));
            $this->entityManager->persist($addressBook);
            $this->entityManager->flush();

            return true;
        }

        return false;
    }
}
